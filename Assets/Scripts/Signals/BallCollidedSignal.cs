using Components;

namespace DefaultNamespace
{
    public class BallCollidedSignal
    {
        public CollisionComponent  CollisionComponent { get; set; }
    }
}