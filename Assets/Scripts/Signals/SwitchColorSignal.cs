namespace DefaultNamespace
{
    public class SwitchColorSignal
    {
        public Direction SwitchDirection;
        public enum Direction
        {
            Next,
            Prev
        }
    }
}