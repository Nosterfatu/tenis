using UnityEngine;

namespace Components
{
    public class CollisionComponent : MonoBehaviour
    {
        [SerializeField] private Kind _kind;

        public Kind ColissionKind => _kind;

        public enum Kind
        {
            TopBoarder,
            BotBoarder,
            TopBoard,
            BotBoard
        }
    }
}