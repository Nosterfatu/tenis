using System;
using Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Systems
{
    public class BoardMoveController : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;


        private void Update()
        {
            if (Input.GetMouseButton(0) == false)
                return;
                
            _signalBus.Fire(new InputMoveSignal()
            {
                Direction =  Input.mousePosition.x > Screen.width/2 ? 1 : -1
            });
        }
    }

}