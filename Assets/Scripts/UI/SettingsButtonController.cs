using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI
{
    public class SettingsButtonController : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;
        [SerializeField] private Button _button;

        private void Start()
        {
            _button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            _signalBus.Fire(new SwitchSettingsViewSignal(){IsActive = true});   
        }
    }
}