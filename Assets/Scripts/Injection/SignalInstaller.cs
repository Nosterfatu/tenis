using DefaultNamespace;
using Signals;
using Zenject;

namespace Injection
{
    public class SignalInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<BallCollidedSignal>();
            Container.DeclareSignal<InputMoveSignal>();
            Container.DeclareSignal<SwitchColorSignal>();
            Container.DeclareSignal<SwitchSettingsViewSignal>();
            Container.DeclareSignal<ResetSignal>();
        }
    }
}