using Data;
using UnityEngine;
using Zenject;

namespace Injection
{
    [CreateAssetMenu (menuName = "Scriptables/Injection/Data", fileName = "DataObjectInstaller")]
    public class DataObjectInstaller : ScriptableObjectInstaller
    {

        [SerializeField] private ScoreDataObject _scoreDataObject;
        [SerializeField] private SettingsDataObject _settingsDataObject;
        public override void InstallBindings()
        {
            Container.Bind<ScoreDataObject>().FromInstance(_scoreDataObject).AsSingle();
            Container.Bind<SettingsDataObject>().FromInstance(_settingsDataObject).AsSingle();
        }
    }
}