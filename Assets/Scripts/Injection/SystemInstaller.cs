using Systems;
using Zenject;

namespace Injection
{
    public class SystemInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<BoardMoveSystem>().AsSingle();
            Container.Bind<GameScoreSystem>().AsSingle();
            Container.Bind<ResetSystem>().AsSingle();
            Container.Bind<SettingsSystem>().AsSingle();
        }
    }
}