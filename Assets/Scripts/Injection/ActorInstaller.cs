using Actors;
using UnityEngine;
using Zenject;

namespace Injection
{
    public class ActorInstaller : MonoInstaller
    {
        [SerializeField] private Ball _ball;
        [SerializeField] private TopBoard _topBoard;
        [SerializeField] private BotBoard _botBoard;
        
        public override void InstallBindings()
        {
            Container.Bind<Ball>().FromInstance(_ball).AsSingle();
            Container.Bind<TopBoard>().FromInstance(_topBoard).AsSingle();
            Container.Bind<BotBoard>().FromInstance(_botBoard).AsSingle();
        }
    }
}