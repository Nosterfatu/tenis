using Systems;
using UI;
using UnityEngine;
using Zenject;

namespace Injection
{
    public class UiInstaller: MonoInstaller
    {
        [SerializeField] private BoardMoveController _boardMoveController; 
        [SerializeField] private SettingsButtonController _settingsButtonController; 
        [SerializeField] private SettingsViewController _settingsViewController;

        public override void InstallBindings()
        {
            Container.Bind<BoardMoveController>().FromInstance(_boardMoveController).AsSingle();
            Container.Bind<SettingsButtonController>().FromInstance(_settingsButtonController).AsSingle();
            Container.Bind<SettingsViewController>().FromInstance(_settingsViewController).AsSingle();
        }
    }
}