using Components;
using UnityEngine;

namespace Actors
{
    public abstract class Board : CollisionComponent
    {
        public void SetPosition(float position)
        {
            transform.position = new Vector3(position, transform.position.y);
        }
    }
}