using System;
using Systems;
using UnityEngine;
using Zenject;

namespace Actors
{
    public class SystemRunner : MonoBehaviour
    {
        [Inject] private BoardMoveSystem _boardMoveSystem;
        [Inject] private GameScoreSystem _gameScoreSystem ;
        [Inject] private ResetSystem _resetSystem;
        [Inject] private SettingsSystem _settingsSystem;

        private void Awake()
        {
            _resetSystem.Reset();
            _settingsSystem.UpdateBallColor();
        }
    }
}