﻿using Components;
using DefaultNamespace;
using UnityEngine;
using Zenject;

namespace Actors
{
    public class Ball : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;
        [SerializeField]
        private SpriteRenderer _renderer;
        
        [SerializeField]
        private Rigidbody2D _rigidbody;

        private void OnTriggerEnter2D(Collider2D other)
        {
            _signalBus.Fire(new BallCollidedSignal(){ CollisionComponent = other.GetComponent<CollisionComponent>()});
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            _signalBus.Fire(new BallCollidedSignal(){ CollisionComponent = other.gameObject.GetComponent<CollisionComponent>()});
        }

        public void SetColor(Color color)
        {
            _renderer.color = color;
        }

        public void SetVelocity(Vector2 velocity)
        {
            _rigidbody.velocity = Vector2.zero;
            _rigidbody.AddForce(velocity,ForceMode2D.Impulse); 
        }
    }
}
