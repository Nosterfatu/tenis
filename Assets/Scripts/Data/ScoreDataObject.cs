using System;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu (menuName = "Scriptables/Data/Score", fileName = "ScoreDataObject")]

    public class ScoreDataObject : ScriptableObject
    {
        [SerializeField] private ScoreData _scoreData;
        public ScoreData ScoreData => _scoreData;
        
    }

    [Serializable]
    public class ScoreData
    {
        public int Score;
        public int MaxScore;
    }
}