using System;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu (menuName = "Scriptables/Data/Settings", fileName = "SettingsDataObject")]

    public class SettingsDataObject : ScriptableObject
    {
        [SerializeField] private SettingsData _settingsData;
        [SerializeField] private Color32[] _colors;
        [SerializeField] private float _ballSpeed;
        [SerializeField] private float _boardSpeed;
        [SerializeField] private float _boardBorder;
        
        public float BoardBorder => _boardBorder;
        public float BallSpeed => _ballSpeed;
        public float BoardSpeed => _boardSpeed;

        public SettingsData SettingsData => _settingsData;

        public Color32[] Colors => _colors;
    }

    [Serializable]
    public class SettingsData
    {
        public int ColorIndex;
    }
}