using Actors;
using Data;
using DefaultNamespace;
using UnityEngine;
using Zenject;

namespace Systems
{
    public class SettingsSystem
    {
        private SettingsViewController _settingsViewController;
        private SettingsDataObject _settingsDataObject;
        private Ball _ball;
        private ScoreData _scoreData;

        [Inject]
        public SettingsSystem(ScoreDataObject scoreDataObject,
            SettingsDataObject settingsDataObject,
            SignalBus signalBus,
            SettingsViewController settingsViewController,
            Ball ball)
        {
            _settingsDataObject = settingsDataObject;
            _settingsViewController = settingsViewController;
            _scoreData = scoreDataObject.ScoreData;
            _ball = ball;
            Subscribe(signalBus);
        }

        public void UpdateBallColor()
        {
            var color = _settingsDataObject.Colors[_settingsDataObject.SettingsData.ColorIndex];
            _ball.SetColor(color);
            _settingsViewController.SetBallColor(color);
        }

        private void Subscribe(SignalBus signalBus)
        {
            signalBus.Subscribe<SwitchSettingsViewSignal>(SwitchPanelView);
            signalBus.Subscribe<SwitchColorSignal>(SwitchColor);
        }

        private void SwitchColor(SwitchColorSignal signal)
        {
            var settingsData = _settingsDataObject.SettingsData;

            if (signal.SwitchDirection == SwitchColorSignal.Direction.Next)
            {
                settingsData.ColorIndex--;
                if (settingsData.ColorIndex < 0)
                    settingsData.ColorIndex = _settingsDataObject.Colors.Length - 1;
            }
            else if (signal.SwitchDirection == SwitchColorSignal.Direction.Prev)
            {
                settingsData.ColorIndex--;
                if (settingsData.ColorIndex >= _settingsDataObject.Colors.Length)
                    settingsData.ColorIndex = 0;
            }

            UpdateBallColor();
        }

        private void SwitchPanelView(SwitchSettingsViewSignal signal)
        {
            Time.timeScale = signal.IsActive? 0 : 1;
            if (signal.IsActive == false)
            {
                _settingsViewController.SetView(false);
                return;
            }

            UpdatePanelView();
            _settingsViewController.SetView(true);
        }

        private void UpdatePanelView()
        {
            UpdateBallColor();
            UpdateScoreView();
        }

        private void UpdateScoreView()
        {
            _settingsViewController.SetScores(_scoreData.Score, _scoreData.MaxScore);
        }

    }
}