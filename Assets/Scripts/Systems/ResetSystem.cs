using Actors;
using Components;
using Data;
using DefaultNamespace;
using Signals;
using UnityEngine;
using Zenject;

namespace Systems
{
    public class ResetSystem
    {
        private Ball _ball;
        private SettingsDataObject _settingsDataObject;
        private ScoreData _scoreData;
        
        public ResetSystem(Ball ball, SignalBus signalBus, SettingsDataObject settingsDataObject, ScoreDataObject scoreDataObject)
        {
            _settingsDataObject = settingsDataObject;
            _ball = ball;
            _scoreData = scoreDataObject.ScoreData;
            
            Subscribe(signalBus);
            Reset();
        }
        
        public void Reset()
        {
            ResetBall();
            _scoreData.Score = 0;
        }

        private void Subscribe(SignalBus signalBus)
        {
            signalBus.Subscribe<BallCollidedSignal>(OnBallCollided);
            signalBus.Subscribe<ResetSignal>(Reset);
        }

        private void OnBallCollided(BallCollidedSignal signal)
        {
            if (signal.CollisionComponent == null 
                ||(signal.CollisionComponent.ColissionKind == CollisionComponent.Kind.BotBoarder
                || signal.CollisionComponent.ColissionKind == CollisionComponent.Kind.TopBoarder) == false)
                return;
           
            Reset();
        }

        private void ResetBall()
        {
            _ball.transform.position = Vector3.zero;
            _ball.SetVelocity(_settingsDataObject.BallSpeed * new Vector2(Random.Range(-100, 100f), Random.Range(-100, 100f)).normalized);
        }

    }
}