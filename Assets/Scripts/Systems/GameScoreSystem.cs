using Components;
using Data;
using DefaultNamespace;
using Zenject;

namespace Systems
{
    public class GameScoreSystem
    {
        private readonly ScoreData _scoreData;
        
        [Inject]
        public GameScoreSystem(SignalBus signalBus, ScoreDataObject scoreDataObject)
        {
            Subscribe(signalBus);
            _scoreData = scoreDataObject.ScoreData;
        }
        public void Subscribe(SignalBus signalBus)
        {
            signalBus.Subscribe<BallCollidedSignal>(OnBallCollided);
        }

        private void OnBallCollided(BallCollidedSignal collidedSignal)
        {
            if (IsBoardCollided(collidedSignal.CollisionComponent) == false)
                return;

            AddScore();
        }

        private void AddScore()
        {
            _scoreData.Score++;
            if (_scoreData.Score > _scoreData.MaxScore)
                _scoreData.MaxScore = _scoreData.Score;
        }

        private bool IsBoardCollided(CollisionComponent component)
        {
            return component != null
                   && (component.ColissionKind == CollisionComponent.Kind.BotBoard
                       || component.ColissionKind == CollisionComponent.Kind.TopBoard);
        }
    }
}