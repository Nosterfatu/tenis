using Actors;
using Data;
using Signals;
using UnityEngine;
using Zenject;

namespace Systems
{
    public class BoardMoveSystem
    {
        private SettingsDataObject _settingsDataObject;

        private Board _topBoard;
        private Board _botBoard;
        
        [Inject]
        public BoardMoveSystem(SettingsDataObject settingsDataObject, SignalBus signalBus, TopBoard topBoard, BotBoard botBoard)
        {
            _settingsDataObject = settingsDataObject;
            signalBus.Subscribe<InputMoveSignal>(MoveBoards);
            _botBoard = botBoard;
            _topBoard = topBoard;
        }

        private void MoveBoards(InputMoveSignal signal)
        {
          
            var isLeftDirection = signal.Direction < 0;

            var addition = (signal.Direction * Time.timeScale * Time.deltaTime * _settingsDataObject.BoardSpeed);
            var position = GetCorrectedPosition(isLeftDirection, addition);
            
            _topBoard.SetPosition(position);
            _botBoard.SetPosition(position);
        }

        private float GetCorrectedPosition(bool isLeft, float addition)
        {
            var currentPosition = _topBoard.transform.position.x;
            var updatedPosition = addition + currentPosition;
            
            if (isLeft && updatedPosition < -_settingsDataObject.BoardBorder)
            {
                return -_settingsDataObject.BoardBorder;
            } 
            
            if (updatedPosition> _settingsDataObject.BoardBorder)
            {
                return _settingsDataObject.BoardBorder;
            }

            return updatedPosition;

        }

        //TODO add net code
        private void SyncBoards()
        {
            
        }
    }
}